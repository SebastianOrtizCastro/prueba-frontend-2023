// Ejercicio 1
// Dada una matriz de N elementos en la que todos los elementos son iguales excepto uno,
// crea una función findUniq que retorne el elemento único.

function findUniq(array) {
    //Se defíne variable a retornar
    let unico; 

    //Se recorre el array de entrada buscando coincidencias
    array.map(foo =>{

        //Si solo un elemento coincide se asigna a la variable a retornar
        if(array.filter(element => element === foo).length===1){
            unico = foo;
        }
    });

    //Retorno de la variable
    return unico;
}


/**
 * TEST Ejercicio 1
 */
findUniq(['12', 10, '12', 11, 1, 11, 10, '12']);// 1

findUniq(['Capitán América', 'Hulk', 'Deadpool', 'Capitán América', 'Hulk', 'Wonder Woman', 'Deadpool', 'Iron Man', 'Iron Man']); // 'Wonder Woman'
