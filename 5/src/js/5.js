
    //Se crea nueva instancia de vue
    const { createApp } = Vue

    //Se Definen los atributos de la instancia
    createApp({
      data() {
        //Se definen los datos a retornar en el domm
        return {
          titulo:'Nuestros usuarios',
          usuarios:null,
          usuario:''
        }
      },
      //Se definen los metodos
      methods:{
        //Se define un metodo que hace una petición y asigna el resultado a this.usuarios
        peticion(){
            axios
            .get('https://jsonplaceholder.typicode.com/users')
            .then(response => (this.usuarios = response.data))
        },
        //Se define el metodo que es llamado al presionar algún usuario
        usuarioPresionado(id){
            const body = document.querySelector('body');
            body.classList.add('fijar-body');
            this.usuarios.forEach(e=>{
                if(e.id===id)this.usuario=e;
            });
            console.log(this.usuario);
        },
        //Se define el metodo que se llama al presionar el botón para cerrar el model de usuario
        cerrarOverlay(){
            this.usuario='';
            const body = document.querySelector('body');
            body.classList.remove('fijar-body');
        }
      },

      //Este metodó se ejecutá al momento de que está montado el dom
      mounted(){
        this.peticion();
        document.querySelector('.d-none').classList.remove('d-none');
      }
      
      //Se define el componente al elemento con id=app
    }).mount('#app')

