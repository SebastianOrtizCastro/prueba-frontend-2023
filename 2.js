// Ejercicio 2
// Dada una matriz de N elementos repetidos,
// crea una función numbersTop para obtener los tres elementos más repetidos ordenados de forma descendente por número de repeticiones.


//Asigna un nombre y un conteo a un objeto y retorna el objeto
function asignar(foo,cuenta){
    const e = {
        nombre:foo,
        conteo:cuenta
    };
    return e;
}

//Funcion para buscar dentro de un array un elemento con nombre === foo retorna true o false
function validar(array, foo){

    let esta = false;

    array.forEach(e=>{
        if(e.nombre===foo)esta = true;
    });

    return esta;
}

function numbersTop(array) {
    //Se define un array para llenar con el conteo de los elementos
    let cuenta=[];

    //Se recorre el array de entrada
    array.forEach(foo =>{

        //Si el arrray que guarda la cuenta está vacio añade el primer elemento
        if(cuenta.length===0){

            //Se llama a la función asignar(nombre,numero de veces que se repite) y te regresa un objeto
            const e = asignar(foo,array.filter(e => e === foo).length);
            cuenta.push(e);
        }

        //Si el array ya tiene elementos valida que ninguno tenga el mismo nombre
        else {

            /*Se llama a la función validar que busca un objeto con el nombre igual a foo, si lo ecuentra
             retorna true y si no*/
            if(!validar(cuenta,foo)){

                //Se llama a la función asignar(nombre,numero de veces que se repite) y te regresa un objeto
                const e = asignar(foo,array.filter(e => e === foo).length);
                cuenta.push(e);
            }
        }
        
    });
    
    //Se ordena el array de objetos con base en su atributo conteo
    cuenta.sort((a,b)=>{

        //Si el elemento es mayor lo coloca una posicion antes
        if (a.conteo > b.conteo) {
            return -1;
          }
        //Si el elemento es menor lo coloca una posicion despues
        if (a.conteo < b.conteo) {
            return 1;
          }

        return 0;
    });

    //Se declara la variable a retornar
    let respuesta=[];

    //Se asignan las tres primeras posiciones del array de la cuenta al array de respuesta
    for(let i = 0;i<3;i++){
        respuesta.push(cuenta[i].nombre)
    }

    return respuesta;
}

/**
 * TEST Ejercicio 2
 */
numbersTop([3, 3, 1, 4, 1, 3, 1, 1, 2, 2, 2, 3, 1, 3, 4, 1]); // [ 1, 3, 2 ]
numbersTop(['a', 3, 2, 'a', 2, 3, 'a', 3, 4, 'a', 'a', 1, 'a', 2, 'a', 3]) // [ 'a', 3, 2 ]
